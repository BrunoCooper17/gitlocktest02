// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "GitLockTest02GameMode.generated.h"

UCLASS(MinimalAPI)
class AGitLockTest02GameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AGitLockTest02GameMode();
};



