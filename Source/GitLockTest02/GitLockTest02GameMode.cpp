// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "GitLockTest02GameMode.h"
#include "GitLockTest02Pawn.h"

AGitLockTest02GameMode::AGitLockTest02GameMode()
{
	// set default pawn class to our character class
	DefaultPawnClass = AGitLockTest02Pawn::StaticClass();
}

