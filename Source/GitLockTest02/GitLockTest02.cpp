// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "GitLockTest02.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, GitLockTest02, "GitLockTest02" );

DEFINE_LOG_CATEGORY(LogGitLockTest02)
 